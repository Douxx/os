<?php
// HTTP
define('HTTP_SERVER', 'http://os/');

// HTTPS
define('HTTPS_SERVER', 'http://os/');

// DIR
define('DIR_APPLICATION', 'C:/OpenServer/domains/opencode/os/catalog/');
define('DIR_SYSTEM', 'C:/OpenServer/domains/opencode/os/system/');
define('DIR_IMAGE', 'C:/OpenServer/domains/opencode/os/image/');
define('DIR_LANGUAGE', 'C:/OpenServer/domains/opencode/os/catalog/language/');
define('DIR_TEMPLATE', 'C:/OpenServer/domains/opencode/os/catalog/view/theme/');
define('DIR_CONFIG', 'C:/OpenServer/domains/opencode/os/system/config/');
define('DIR_CACHE', 'C:/OpenServer/domains/opencode/os/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OpenServer/domains/opencode/os/system/storage/download/');
define('DIR_LOGS', 'C:/OpenServer/domains/opencode/os/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OpenServer/domains/opencode/os/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OpenServer/domains/opencode/os/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'osx');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
